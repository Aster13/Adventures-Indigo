#===============================================================================
# Battle Card plugin
# For Essentials v20
# By sorryjzargo
#===============================================================================

ItemHandlers::UseInField.add(:BATTLECARD,proc { |item|
  pbMessage(_INTL("You have {1} BP.",$Trainer.battle_points.to_s_formatted))
  next 1
})