#---------------------------------------#
#         Lavender Town's Ghost         #
#---------------------------------------#
# zerokid (original), bo4p5687 (update)
#---------------------------------------#

module Ghost
	GHOST_MAPS = []
	# Check event ghost
	def self.active?
		return true if !$bag.has?(:SILPHSCOPE) && GHOST_MAPS.include?($game_map.map_id)
		return false
	end
end

# Change bitmap
class Battle::Scene::BattlerSprite
	# Set: def setPokemonBitmap(pkmn, back = false)
  def set_ghost_bitmap
    @_iconBitmap&.dispose
    @_iconBitmap = AnimatedBitmap.new("Graphics/Pictures/Ghost.png")
    self.bitmap = (@_iconBitmap) ? @_iconBitmap.bitmap : nil
    pb_set_position_ghost
  end

	# Set position: def pbSetPosition
  def pb_set_position_ghost
		return if !@_iconBitmap
    pbSetOrigin
    if @index.even?
      self.z = 50 + (5 * @index / 2)
    else
      self.z = 50 - (5 * (@index + 1) / 2)
    end
    # Set original position
    p = Battle::Scene.pbBattlerPosition(@index, @sideSize)
    @spriteX = p[0]
    @spriteY = p[1]
  end

	alias ghost_intro_animation pbPlayIntroAnimation
	def pbPlayIntroAnimation(pictureEx = nil)
		return if Ghost.active?
    ghost_intro_animation(pictureEx)
  end
end