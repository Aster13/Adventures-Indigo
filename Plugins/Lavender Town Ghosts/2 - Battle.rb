class Battle::Scene
  # Set bitmap: def pbChangePokemon(idxBattler, pkmn)
  def pb_change_ghost_pokemon(idxBattler, pkmn)
    idxBattler = idxBattler.index if idxBattler.respond_to?("index")
    pkmnSprite = @sprites["pokemon_#{idxBattler}"]
    pkmnSprite.set_ghost_bitmap
  end

	# Init sprite
	alias ghost_init_sprite pbInitSprites
	def pbInitSprites
		ghost_init_sprite
		# Wild battle, so set up the Pokémon sprite(s) accordingly
		if @battle.wildBattle?
			@battle.pbParty(1).each_with_index { |pkmn, i|
				index = (i * 2) + 1
				# Ghost event
				Ghost.active? ? pb_change_ghost_pokemon(index, pkmn) : pbChangePokemon(index, pkmn)
				pkmnSprite = @sprites["pokemon_#{index}"]
				pkmnSprite.tone    = Tone.new(-80, -80, -80)
				pkmnSprite.visible = true
			}
    end
	end
end

#---------------------------------------------------------------------------------------------------------------#
# Battle
class Battle
	# Set run
	alias ghost_run pbRun
	def pbRun(idxBattler, duringBattle = false)
		battler = @battlers[idxBattler]
		return ghost_run(idxBattler, duringBattle) if battler.opposes? || trainerBattle? || ($DEBUG && Input.press?(Input::CTRL))
		# New
		if !duringBattle && Ghost.active?
			pbSEPlay("Battle flee")
			pbDisplayPaused(_INTL("You got away safely!"))
			@decision = 3
			return 1
		end
		ghost_run(idxBattler, duringBattle)
	end

	#--------------------------#
	# Battle
	alias ghost_start_battle_send_out pbStartBattleSendOut
	def pbStartBattleSendOut(sendOuts)
		return pb_start_battle_send_out_ghost(sendOuts) if wildBattle? && Ghost.active?
		return ghost_start_battle_send_out(sendOuts)
	end

	# New: send out
	def pb_start_battle_send_out_ghost(sendOuts)
		return unless (wildBattle? && Ghost.active?)
		pbDisplayPaused(_INTL("Ghostttt!"))
    # Send out Pokémon (opposing trainers first)
		[1, 0].each { |side|
			next if side == 1 && wildBattle?
			msg = ""
			toSendOut = []
			trainers = (side == 0) ? @player : @opponent
			# Opposing trainers and partner trainers's messages about sending out Pokémon
			trainers.each_with_index { |t, i|
				next if side == 0 && i == 0   # The player's message is shown last
				msg += "\r\n" if msg.length > 0
				sent = sendOuts[side][i]
				case sent.length
				when 1
					msg += _INTL("{1} sent out {2}!", t.full_name, @battlers[sent[0]].name)
				when 2
					msg += _INTL("{1} sent out {2} and {3}!", t.full_name, @battlers[sent[0]].name, @battlers[sent[1]].name)
				when 3
					msg += _INTL("{1} sent out {2}, {3} and {4}!", t.full_name, @battlers[sent[0]].name, @battlers[sent[1]].name, @battlers[sent[2]].name)
				end
				toSendOut.concat(sent)
			}
			# The player's message about sending out Pokémon
			if side == 0
				msg += "\r\n" if msg.length > 0
				sent = sendOuts[side][0]
				case sent.length
				when 1
					msg += _INTL("Go! {1}!", @battlers[sent[0]].name)
				when 2
					msg += _INTL("Go! {1} and {2}!", @battlers[sent[0]].name, @battlers[sent[1]].name)
				when 3
					msg += _INTL("Go! {1}, {2} and {3}!", @battlers[sent[0]].name, @battlers[sent[1]].name, @battlers[sent[2]].name)
				end
				toSendOut.concat(sent)
			end
			pbDisplayBrief(msg) if msg.length > 0
			# The actual sending out of Pokémon
			animSendOuts = []
			toSendOut.each { |idxBattler|
				animSendOuts.push([idxBattler, @battlers[idxBattler].pokemon])
			}
			pbSendOut(animSendOuts, true)
		}
	end
	#--------------------------#

	# Pokemon can't fight
	alias ghost_fight_menu pbFightMenu
	def pbFightMenu(idxBattler) = Ghost.active? && wildBattle? ? pbDisplayPaused(_INTL("{1} is too scared to moveeee!", @battlers[idxBattler].name)) : ghost_fight_menu(idxBattler)
end

#---------------------------------------------------------------------------------------------------------------#
# Throw Pokeball
module Battle::CatchAndStoreMixin
  alias throw_poke_ball_ghost pbThrowPokeBall
	def pbThrowPokeBall(idxBattler, ball, catch_rate = nil, showPlayer = false)
    if Ghost.active?
			@scene.pbThrowAndDeflect(ball, idxBattler)
      pbDisplayPaused(_INTL("It dodged the thrown Ball! This POKéMON can't be caught!"))
		else
			throw_poke_ball_ghost(idxBattler, ball, catch_rate, showPlayer)
    end
  end
end

#---------------------------------------------------------------------------------------------------------------#
class Battle::AI
  alias choose_enemy_command_ghost pbDefaultChooseEnemyCommand
  def pbDefaultChooseEnemyCommand(idxBattler)
    if Ghost.active? && @battle.wildBattle? && @battle.opposes?(idxBattler)
      @battle.pbDisplayPaused(_INTL("Ghost: Get out... Get out..."))
		else
			choose_enemy_command_ghost(idxBattler)
    end
  end
end

#---------------------------------------------------------------------------------------------------------------#
# Init pokemon
class Battle::Battler
	alias ghost_init_pokemon pbInitPokemon
  def pbInitPokemon(pkmn, idxParty)
		ghost_init_pokemon(pkmn, idxParty)
		@name   = "Ghost" if opposes? && !@battle.trainerBattle? && Ghost.active?
  end
end

#---------------------------------------------------------------------------------------------------------------#
# Draw data box
class Battle::Scene::PokemonDataBox
	alias ghost_refresh refresh
  def refresh
    self.bitmap.clear
    return if !@battler.pokemon
		if @battler.opposes?(0) && Ghost.active? && @battler.wild?
			textPos = []
			imagePos = []
			# Draw background panel
			draw_background
			# Draw Pokémon's name
			draw_name
			# Draw Pokémon's level
			draw_level
			refreshHP
			refreshExp
		else
			ghost_refresh
		end
  end
end